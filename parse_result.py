#!python3

import re
import tabulate
import xml.etree.ElementTree as ET

tabulate.PRESERVE_WHITESPACE = True

statistics = {}
compiler = ['C', 'CXX', 'FC', '']
omp = ['OMP45', 'OMP50', 'OMP51', '']
status = ['run', 'notrun', 'fail']
for c in compiler:
    statistics[c] = {}
    for o in omp:
        statistics[c][o] = {}
        for s in status:
            statistics[c][o][s] = 0

ctest_results = ET.parse('junit.xml').getroot()
for child in ctest_results:
    match = re.match(r'([A-Z]*)_([A-Z0-9]*)_.*', child.attrib['name'])
    if match:
        statistics[match.group(1)][match.group(2)][child.attrib['status']] += 1
        statistics[match.group(1)][''][child.attrib['status']] += 1
        statistics[''][match.group(2)][child.attrib['status']] += 1
        statistics[''][''][child.attrib['status']] += 1

info = {'': omp,
        'C': [f"{statistics['C'][o]['run']: >3} / {statistics['C'][o]['notrun']:>3} / {statistics['C'][o]['fail']:>3}" for o in omp],
        'CXX': [f"{statistics['CXX'][o]['run']: >3} / {statistics['CXX'][o]['notrun']:>3} / {statistics['CXX'][o]['fail']:>3}" for o in omp],
        'FC': [f"{statistics['FC'][o]['run']: >3} / {statistics['FC'][o]['notrun']:>3} / {statistics['FC'][o]['fail']:>3}" for o in omp],
        ' ': [f"{statistics[''][o]['run']: >3} / {statistics[''][o]['notrun']:>3} / {statistics[''][o]['fail']:>3}" for o in omp]}
print(tabulate.tabulate(info, headers='keys', tablefmt='pretty'))

