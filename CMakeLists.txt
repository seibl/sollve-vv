cmake_minimum_required(VERSION 3.20)
project(sollve_vv 
   VERSION 1.0)

include(CMakePrintHelpers)

option(SOLLVE_ENABLE_C "Enable C language tests." ON)
option(SOLLVE_ENABLE_CXX "Enable C++ language tests." ON)
option(SOLLVE_ENABLE_FC "Enable Fortran language tests." ON)

option(SOLLVE_ENABLE_OMP45 "Enable OMP version 4.5 tests" ON)
option(SOLLVE_ENABLE_OMP50 "Enable OMP version 5.0 tests" ON)
option(SOLLVE_ENABLE_OMP51 "Enable OMP version 5.1 tests" ON)

set(SOLLVE_C_OPTIONS "" CACHE STRING "Compiler options for C compiler")
set(SOLLVE_CXX_OPTIONS "" CACHE STRING "Compiler options for CXX compiler")
set(SOLLVE_FC_OPTIONS "" CACHE STRING "Compiler options for FC compiler")

if(CMAKE_PROJECT_NAME STREQUAL PROJECT_NAME)
    include(CTest)
endif()

function(generate_tests COMPILER OMP LIB SOURCES)
   set(i 0)
   foreach(TEST_FULL_SRC ${SOURCES})
      math(EXPR i "${i} + 1")
      cmake_path(GET TEST_FULL_SRC FILENAME TEST_SRC)
      set(TEST_BIN ${COMPILER}_${OMP}_${TEST_SRC}_bin)

      add_executable(${TEST_BIN})
      target_sources(${TEST_BIN} PRIVATE ${TEST_FULL_SRC})
      target_link_libraries(${TEST_BIN} PRIVATE ${LIB})
      add_test(NAME ${COMPILER}_${OMP}_${TEST_SRC} COMMAND ${TEST_BIN})
      set_tests_properties(${COMPILER}_${OMP}_${TEST_SRC} PROPERTIES LABELS "${COMPILER};${OMP}")
   endforeach()
endfunction()

file(CREATE_LINK ${CMAKE_CURRENT_SOURCE_DIR}/parse_result.py ${CMAKE_CURRENT_BINARY_DIR}/parse_result.py SYMBOLIC)

include(cmake/c.cmake)
include(cmake/cxx.cmake)
include(cmake/fc.cmake)
