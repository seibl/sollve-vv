if (SOLLVE_ENABLE_FC)
    enable_language(Fortran)

    add_library(ompvv_fc)
    target_sources(ompvv_fc PRIVATE ${CMAKE_SOURCE_DIR}/ompvv/ompvv.F90)
    target_compile_definitions(ompvv_fc PUBLIC VERBOSE_MODE)
    target_include_directories(ompvv_fc PUBLIC ${CMAKE_SOURCE_DIR}/ompvv)
    target_link_libraries(ompvv_fc PUBLIC m)

    if (SOLLVE_FC_OPTIONS STREQUAL "")
        target_compile_options(ompvv_fc PUBLIC -mp=gpu)
        target_link_options(ompvv_fc PUBLIC -mp=gpu)
    else ()
        target_compile_options(ompvv_fc PUBLIC ${SOLLVE_FC_OPTIONS})
        target_link_options(ompvv_fc PUBLIC ${SOLLVE_FC_OPTIONS})
    endif ()

    cmake_print_properties(TARGETS ompvv_fc
            PROPERTIES COMPILE_DEFINITIONS COMPILE_OPTIONS)

    if (SOLLVE_ENABLE_OMP45)
        file(GLOB_RECURSE OMP_45_FC_SOURCES tests/4.5/*.F90)
    endif ()
    if (SOLLVE_ENABLE_OMP50)
        file(GLOB_RECURSE OMP_50_FC_SOURCES tests/5.0/*.F90)
    endif ()
    if (SOLLVE_ENABLE_OMP51)
        file(GLOB_RECURSE OMP_51_FC_SOURCES tests/5.1/*.F90)
    endif ()

    generate_tests("FC" "OMP45" ompvv_fc "${OMP_45_FC_SOURCES}")
    generate_tests("FC" "OMP50" ompvv_fc "${OMP_50_FC_SOURCES}")
    generate_tests("FC" "OMP51" ompvv_fc "${OMP_51_FC_SOURCES}")
endif ()
